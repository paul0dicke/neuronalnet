import json
import math
import pickle
import random

import numpy as np

from NN.Visualize import Visualize


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def desigmoid(y):
    return y * (1 - y)


def map(array, func):
    for i, c in enumerate(array):
        for j, _r in enumerate(c):
            array[i][j] = func(array[i][j])
    return array


class NeuronalNetwork(Visualize):
    def __init__(self):

        super().__init__()
        # load json settings
        with open("setup.json") as json_file:
            settings = json.load(json_file)
            json_file.close()

        # set all from json file
        self.shape = settings["shape"]
        self.learning_rate = settings["learning_rate"]
        self.weights_min = settings["weights_min"]
        self.weights_max = settings["weights_max"]
        self.bias_min = settings["bias_min"]
        self.bias_max = settings["bias_max"]
        self.inputs_size = self.shape[0]
        self.net_size = len(self.shape)
        self.visual = None

        self.batch_size = settings["batch_size"]
        self.test_size = settings["test_size"]
        self.iterations = settings["iterations"]

        self.weights = []
        self.biases = []

        for i in range(1, len(self.shape)):
            current_weights = np.random.uniform(low=self.weights_min, high=self.weights_max,
                                                size=(self.shape[i], self.shape[i - 1]))
            current_biases = np.random.uniform(low=self.bias_min, high=self.bias_max, size=(self.shape[i], 1))
            self.weights.append(current_weights)
            self.biases.append(current_biases)

    def feedforward(self, inputs):

        if self.inputs_size != len(inputs):
            raise ValueError(
                "Wrong input size for Net, expected: " + str(self.inputs_size) + " got " + str(len(inputs)))

        inputs = np.transpose(np.asmatrix(inputs))

        for i, current_weights in enumerate(self.weights):
            inputs = np.matmul(current_weights, inputs) + self.biases[i]
            inputs = map(inputs, sigmoid)

        return inputs

    def train(self, inputs, targets, evaluate_inputs=None, evaluate_targets=None, metrics=False):

        for i in range(self.iterations):
            print("********************************")
            print("Running Batch " + str(i + 1) + "/" + str(self.iterations))

            train_data = random.sample(list(zip(inputs, targets)), self.batch_size)
            train_images_batch, train_labels_batch = zip(*train_data)

            evaluate_data = random.sample(list(zip(evaluate_inputs, evaluate_targets)), self.test_size)
            evaluate_images_batch, evaluate_labels_batch = zip(*evaluate_data)

            self._train_batch(train_images_batch, train_labels_batch, evaluate_images_batch, evaluate_labels_batch,
                              metrics)

            print("Error: " + str(self.mean_error[-1]))
            print()

        self.show_mean_error()

        file_to_store = open("net.pickle", "wb")
        pickle.dump(self, file_to_store)
        file_to_store.close()

    def _train_batch(self, inputs, target, evaluate_input, evaluate_target, metrics=False):

        joined_lists = list(zip(inputs, target))
        random.shuffle(joined_lists)
        inputs, target = zip(*joined_lists)

        for index in range(len(inputs)):
            self._backpropagation(inputs[index], target[index])

        # Evaluate
        if metrics:
            joined_lists = list(zip(evaluate_input, evaluate_target))
            random.shuffle(joined_lists)
            evaluate_input, evaluate_target = zip(*joined_lists)
            mean_error = 0
            for index in range(len(evaluate_input)):
                out = self.feedforward(evaluate_input[index])
                out = np.asarray(out)
                for i, x in enumerate(out):
                    mean_error += abs(x - evaluate_target[index][i])

            mean_error = mean_error / len(evaluate_input)
            self.mean_error.append(mean_error)

    def _backpropagation(self, inputs, targets):
        inputs = np.transpose(np.asmatrix(inputs))
        targets = np.transpose(np.asmatrix(targets))

        outputs = [inputs]

        for i, current_weights in enumerate(self.weights):
            inputs = np.matmul(current_weights, inputs) + self.biases[i]
            inputs = map(inputs, sigmoid)
            outputs.append(inputs)

        """Start back propagation"""
        # calc Errors for output
        output_errors = np.subtract(targets, outputs[-1])

        # calculate Gradient, the Errors * (derivative from Sigmoid(outputs))
        gradient = map(outputs[-1], desigmoid)
        gradient = np.multiply(gradient, output_errors)

        # * learningrate
        gradient = np.multiply(gradient, self.learning_rate)

        # adjust bias
        self.biases[-1] += gradient

        # calculate delta + set weights: INPUT * gradient
        input_transposed = np.transpose(outputs[-2])
        delta_hidden_to_out = np.matmul(gradient, input_transposed)
        self.weights[-1] += delta_hidden_to_out

        for i in range(self.net_size - 2, 0, -1):
            index = i - 1
            weights_layer_before_transposed = np.transpose(self.weights[index + 1])
            current_error = np.matmul(weights_layer_before_transposed, output_errors)

            current_gradient = map(outputs[index + 1], desigmoid)

            current_gradient = np.multiply(current_gradient, current_error)
            current_gradient = np.multiply(current_gradient, self.learning_rate)

            self.biases[index] += current_gradient

            input_to_layer_transposed = np.transpose((outputs[index]))
            current_delta = np.matmul(current_gradient, input_to_layer_transposed)
            self.weights[index] += current_delta

            # pass the error to the next Layer
            output_errors = current_error

import sys

import networkx as nx
import matplotlib.pyplot as plt


class Visualize():

    def __init__(self):
        self.mean_error = []

    def show_mean_error(self):
        plt.plot(range(len(self.mean_error)), self.mean_error)
        plt.xlabel("Epochs")
        plt.ylabel("mean absolute error")
        plt.draw()  # pyplot draw()
        plt.show()

    def draw_net(self):

        count = 0
        nodes_count = []
        max_count = max(self.shape)
        G = nx.Graph()
        for i in range(len(self.shape)):
            current_nodes = []
            for j in range(self.shape[i]):
                count += 1
                start = max_count / self.shape[i]
                if self.shape[i] == 1:
                    start = max_count/2 + 0.5
                G.add_node(count, pos=(i, j + start))
                current_nodes.append(count)
            nodes_count.append(current_nodes)


        for i in range(len(nodes_count) - 1):
            current_weigths = self.weights[i]
            row = 0
            for j in nodes_count[i]:

                for num,k in enumerate(nodes_count[i + 1]):
                    normalized_weigth =  (1/(current_weigths[num][row]**2 + 1)) * 0.1
                    color = [normalized_weigth,normalized_weigth,normalized_weigth]
                    G.add_edge(j,k, color=color, weight=round(current_weigths[num][row],2))

                row += 1
        colors = nx.get_edge_attributes(G, 'color').values()


        pos = nx.get_node_attributes(G, 'pos')
        nx.draw(G, pos, edge_color=colors)
        labels = nx.get_edge_attributes(G, 'weight')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=labels, font_size=10, label_pos=0.8)
        plt.draw()  # pyplot draw()
        plt.show()








import random

import numpy as np
from matplotlib import pyplot as plt
from mlxtend.data import loadlocal_mnist

from NN import NeuronalNetwork

nn = NeuronalNetwork()

train_images, train_labels = loadlocal_mnist(
    images_path=r'E:\DataSet MNIST\train-images.idx3-ubyte',
    labels_path=r'E:\DataSet MNIST\train-labels.idx1-ubyte')

evaluate_images, evaluate_labels = loadlocal_mnist(
    images_path=r'E:\DataSet MNIST\t10k-images.idx3-ubyte',
    labels_path=r'E:\DataSet MNIST\t10k-labels.idx1-ubyte')

train_images = train_images / 255
evaluate_images = evaluate_images / 255

# Make output array
train_labels_clean = []
for i, label in enumerate(train_labels):
    out = [0 for y in range(10)]
    out[label] = 1
    train_labels_clean.append(out)
train_labels = train_labels_clean

evaluate_labels_for_check = evaluate_labels
evaluate_labels_clean = []
for i, label in enumerate(evaluate_labels):
    out = [0 for y in range(10)]
    out[label] = 1
    evaluate_labels_clean.append(out)
evaluate_labels = evaluate_labels_clean

nn.train(train_images, train_labels, evaluate_images, evaluate_labels, metrics=True)

richtig = 0
for index, value in enumerate(evaluate_labels_clean):
    out = np.asarray(nn.feedforward(evaluate_images[index]))
    out = out.flatten()

    max_v = out[0]
    max_index = 0

    for i, v in enumerate(out):

        if v > max_v:
            max_v = v
            max_index = i
    if max_index == evaluate_labels_for_check[index]:
        richtig += 1

print("Von " + str(len(evaluate_labels_clean)) + " sind " + str(richtig) + " richtig!")
print("Das sind " + str(richtig / len(evaluate_labels_clean) * 100) + "%")

evaluate_images_batch = []
evaluate_labels_batch = []
for i in range(10):
    index = random.randint(0, len(evaluate_labels) - 1)
    evaluate_images_batch.append(evaluate_images[index])
    evaluate_labels_batch.append(evaluate_labels[index])

for i, pic in enumerate(evaluate_images_batch):
    first_image = np.array(pic, dtype='float')
    pixels = first_image.reshape((28, 28))
    plt.imshow(pixels, cmap='gray')
    out = np.asarray(nn.feedforward(pic))
    out = out.flatten()

    max_v = out[0]
    max_index = 0

    for i, v in enumerate(out):

        if v > max_v:
            max_v = v
            max_index = i

    print("NN-Out: " + str(max_index))
    plt.show()
